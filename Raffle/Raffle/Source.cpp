#include "Utilities.h"

using namespace std;
void raffle()
{
	int tickets;
	cout << "Please enter the amount of tickets you are raffling." << endl;
	cout << "> ";
	cin >> tickets;
	int ticketDrawn = rand() % tickets + 1;
    if (ticketDrawn == 0){
        ticketDrawn += 1;
    }
	cout << "Ticket #" << ticketDrawn << " was drawn." << endl;
	cin.ignore();
	cin.clear();
	cin.get();
}

void welcomeScreen()
{
	int selectedOption;
	cout << "Welcome to the ticket raffler." << endl;
	cout << "Type '1' and then enter to see instructions." << endl;
	cout << "Type '2' and then enter to run the program." << endl;
	cout << "> ";
	cin >> selectedOption;
	if (selectedOption == 2)
	{
		raffle();
	}
	else
	{
		int doneWithInstruction;
		cout << "To use the program, type the number of tickets you are raffling and then press enter." << endl;
		cout << "The first ticket will be seen as ticket #1 by the program." << endl;
		cout << "The second ticket will be seen ticket #2, the third will be seen as ticket #3, and so on." << endl;
		cout << "All you have to do is count your tickets up until the ticket drawn by the program, then see who the ticket belongs to." << endl;
		cout << "Type '1' and then enter to proceed." << endl;
		cout << "> ";
		cin >> doneWithInstruction;
		if (doneWithInstruction == 1)
		{
			raffle();
		}
	}
}



int main()
{

	srand(time(NULL));
	welcomeScreen();

}
